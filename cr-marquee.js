angular.module('corley.jquery-marquee', [])
    .directive('crMarquee', function(){
        return {
            restrict: 'EAC',
            link: function(scope, el, attr){
                    $(el).marquee({
                        duration: attr.duration,
                        direction: attr.direction,
                        duplicated: attr.duplicated
                    });
            } 
        };
    });
